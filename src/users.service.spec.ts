import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UserRepository } from './user.repository';
import { UsersService } from './users.service';

describe('UsersService', () => {
  let usersService: UsersService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: getRepositoryToken(UserRepository),
          useValue: {
            find() { return Promise.resolve('ok') }
          }
        }
      ],
    }).compile();

    usersService = app.get<UsersService>(UsersService);
  });

  it('findAll should return "ok"', async () => {
    const whenFindAll = usersService.findAll();

    await expect(whenFindAll).resolves.toBe('ok');
  });
});
