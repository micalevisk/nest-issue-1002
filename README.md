## Installation

```bash
npm install
```

## Running the app

```bash
docker-compose up -d

# development
npm run start

# watch mode
npm run start:dev

# production mode
npm run start:prod
```

## Test

```bash
# unit tests
npm run test
```

## Connect to the mysql database 

```bash
docker run --net=nest-typeorm-issue-984_default -it --rm mysql:8 mysql -h db -u root -p
# and type the password: root
```
